import Message from "../../models/messages";

const Mutation = {

    createMessage:async(_,{title,content,author}) => {
         const nMessage = new Message({title,content,author})
       return await  nMessage.save()
    },
    deleteMessage:async(_,{id}) => {
        return await Message.findByIdAndDelete(id);
    }

}

export default Mutation;