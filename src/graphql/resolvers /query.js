import Message from "../../models/messages";
const Query = {
    message: async ()=>{
      return  await Message.find();
    },
    messageId: async(_,{id}) => {
        return await Message.findById(id)
     }

}

export default Query;